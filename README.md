AULA 08: FIM
1. Alterei o src/componentes/InputCustomizado para utlilzar spread operator
2. Foi gerado uma vesão de build para prod.
	npm run build
	para executar: npm install pushstate-server

AULA 07:
1. Criei o arquivo src/Livro.js para listar os livros
2. Alterei o index.js para criar a rota para os livros

AULA 06:
Objetivo: Criar rotas para a aplicação

1. Instale o react-router 
	npm install react-router@2.7.0 --save
2. Crie o arquivo Home.js em src
3. Altere a classe AutorBox em src/Autor.js
4. Altere o links existentes em App.js
5. Por fim implemente as rotas no arquivo index.js

Atualização react-router:

1. Instale a versão 4 do react router
	npm install react-router-dom@4.1.1 --save
2. Altere os arquivos src/index.js e src/App.js para a nova versão

AULA 05:
Objetivo: Isolar os componentes de forma mais organizada e realizar validações.

1. Fazer a instalação do pubsub-js isso vai nos ajudar a trabalhar com event publishers
	npm install --save pubsub-js@1.5.3
2. Criar o arquivo Autor.js em src
3. Criar o arquivo TratadorErros.js em src
4. Alterar o arquivo src/componentes/InputCustomizado.js
5. Editar o arquivo App.js para utilizar a nova estrutura	

AULA 04:
Objetivo fazer o cadastro de novos autores e exibir a lista sem precisar atualizar a página.

1. Em src crie um pasta chamada componentes
2. Dentro dessa pasta crie o componente InputCustomizado.js
3. Altere o App.js para utilizar o componente criado

Exercício: Criando componente customizado para botão de submit
1. Na pasta src/componentes crie o arquivo BotaoSubmitCustomizado.js
2. Altere o arquivo src/App.js para utilizar o botão customizado

AULA 03:
ATENÇÃO:
Você deve ter o MySQL instalado e em funcionamento
Crie um usuario no MySQL
mysql> CREATE USER 'alura'@'%' IDENTIFIED BY 'alura';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'alura'@'%';
mysql> FLUSH PRIVILEGES
Utilize o jar desse commit para simular o backend:
https://bitbucket.org/jorge_rabello/cdc-admin/commits/16eadfa7d1d0f0a217134a9fd904dad142059ca2

> Instalar o jquery
> No diretório cdc-admin executar
	npm install jquery --save
> Importar o jquery no App.js
	import $ from 'jquery';

> O react tem método de ciclo de vida:
componentDidMount() 	-> Chamada depois da função render em App.js
componentWillMount() 	-> Chamada antes da função render em App.js

> Alterei o App.js para carregar a lista dinamicamente a partir dos dados da api
> Foi necessário fazer um cadastro de um autor para testar


AULA 02:
> Baixar o css de layout
	https://github.com/pure-css/pure-release/archive/v1.0.0.zip
> Baixar o segundo arquivo de layout
	https://purecss.io/layouts/side-menu/download
> Criar o diretório css em cdc-admin/src/
> Copiar os arquivos pure-min.css e side-menu.css para o diretório cdc-admin/src/css
> Fazer o import dos css em cdc-admin/src/App.js
> Remover os imports
import logo from './logo.svg'; e 
import './App.css';
> Fazer o mock da tela em App.js na função render()

AULA 01:

> Instalar o NodeJS https://nodejs.org/en
> Instalar o create-react app 
	sudo npm install -g create-react-app
> Acessar o diretório do projeto
	cd cdc-admin/
> Iniciar o app
	npm start